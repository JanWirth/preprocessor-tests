var gulp = require('gulp');
var watch = require('gulp-watch');
var path = require('path');
var less = require('gulp-less');
var sass = require('gulp-ruby-sass');
var stylus = require('gulp-stylus');



gulp.task('less', function() {
  return gulp.src('./tests/source/*.less')
    .pipe(less())
    .pipe(gulp.dest('./tests/results/less'));
});
gulp.task('sass', function() {
  return gulp.src('./tests/source/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('./tests/results/sass'));
});
gulp.task('stylus', function() {
  return gulp.src('./tests/source/*.styl')
    .pipe(stylus())
    .pipe(gulp.dest('./tests/results/stylus'));
});